//
//  PermutationMatrix.cpp
//  Yandex.Algorithm
//
//  Created by moxxim on 2/18/18.
//  Copyright © 2018 moxxim. All rights reserved.
//

#include "PermutationMatrix.hpp"
#include <fstream>

// Description
/*
 Владислав придумал новый алгоритм построения распределенного ключа.
 Распределенный ключ состоит из 2n численных последовательностей длины n. Для построение ключа используется случайная перестановка p из n^2 элементов. Элементы перестановки p записываются построчно в квадратную матрицу A. Первые n элементов записываются в первую строку матрицы, следующие n элементов записываются во вторую строку и т.д. Затем выписываются 2n последовательностей длины n — строки и столбцы матрицы a.
 Владиславу даны эти 2n последовательностей в каком-то произвольном порядке. Он долго думал над тем, чтобы придумать алгоритм для восстановления перестановки. Успейте восстановить перестановку до конца раунда!
 1 sec 512Mb
*/

// Input
/*
 В первой строке входных данных записано одно целое число n (1 ≤ n ≤ 100).
 Далее в каждой из 2n строк записано n целых чисел от 1 до n^2 — строки и столбцы матрицы a в некотором порядке.
 Гарантируется, что входные данные корректны, т.е. существует не менее одной подходящей перестановки p.
*/

// Output
/*
 Выведите перестановку чисел от 1 до n^2, которую вы восстановили.
*/

// Example
/*
 2
 1 2
 1 3
 2 4
 3 4
 
 =>
 
 1 2 3 4
*/

int RunPermutationTest()
{
    // Open files.

    std::ifstream in("input.txt");
    if(!in)
    {
        return 1;
    }

    std::ofstream out("output.txt");
    if(!out)
    {
        return 1;
    }

    // Read file and decypher.

    unsigned dim;
    in >> dim;
    unsigned *first = new unsigned[dim];
    unsigned **mat2 = new unsigned*[dim];

    for(unsigned i = 0; i < (2 * dim); ++i)
    {
        unsigned *line = new unsigned[dim];
        if(i == 0)
        {
            for (unsigned j = 0; j < dim; ++j)
            {
                unsigned num;
                in >> num;
                line[j] = num;
            }

            first = line;
        }
        else
        {
            bool found = false;
            for (unsigned j = 0; j < dim; ++j)
            {
                unsigned value;
                in >> value;
                line[j] = value;

                if(!found)
                {
                    int index = -1;
                    for(unsigned k = 0; k < dim; ++k)
                    {
                        if(value == first[k])
                        {
                            index = k;
                            break;
                        }
                    }

                    if(index >= 0)
                    {
                        mat2[index] = line;
                        found = true;
                    }
                }
            }

            if(!found)
            {
                delete [] line;
            }
        }
    }

    // Write output.

    for(unsigned i = 0U; i < dim; ++i)
    {
        for (unsigned j = 0U; j < dim; ++j)
        {
            out << mat2[i][j] << " ";
        }
    }

    // Dispose resources.

    for(unsigned i = 0U; i < dim; ++i)
    {
        delete [] mat2[i];
    }
    
    delete [] first;
    delete [] mat2;
    
    return 0;
}
