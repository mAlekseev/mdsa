//
//  TwoSums.cpp
//  Yandex.Algorithm
//
//  Created by moxxim on 5/14/18.
//  Copyright © 2018 moxxim. All rights reserved.
//

/*
 Given an array of integers, return indices of the two numbers such that they add up to a specific target.

 You may assume that each input would have exactly one solution, and you may not use the same element twice.
 Ex.
 Given nums = [2, 7, 11, 15], target = 9,

 Because nums[0] + nums[1] = 2 + 7 = 9,
 return [0, 1].
 */

#include "TwoSums.hpp"
#include <iostream>
#include <vector>

using namespace std;

vector<int> twoSum(vector<int>& nums, int target)
{
    vector<int> out;
    auto inCount = nums.size();
    for(unsigned i = 0U; i < inCount - 1; ++i)
    {
        int first = nums[i];
        out.push_back(i);
        for(unsigned j = i + 1; j < inCount; ++j)
        {
            int second = nums[j];
            if(first + second == target)
            {
                out.push_back(j);
                return out;
            }
        }
        
        out.clear();
    }

    return out;
}

void TestTwoSums()
{
    vector<int> in = {2, 7, 11, 5};
    vector<int> out = twoSum(in, 9);

    cout << out[0] << ", " << out[1] << std::endl;
}
