//
//  AddTwoNumbers.cpp
//  Yandex.Algorithm
//
//  Created by moxxim on 5/14/18.
//  Copyright © 2018 moxxim. All rights reserved.
//

/*
 You are given two non-empty linked lists representing two non-negative integers. The digits are stored in reverse order and each of their nodes contain a single digit. Add the two numbers and return it as a linked list.

 You may assume the two numbers do not contain any leading zero, except the number 0 itself.
 
 Ex.
 Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
 Output: 7 -> 0 -> 8
 Explanation: 342 + 465 = 807.
 */

#include "AddTwoNumbers.hpp"
#include <iostream>

struct ListNode
{
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

ListNode * GetL1()
{
    ListNode *l1 = new ListNode(2);
    ListNode *l2 = new ListNode(4);
    ListNode *l3 = new ListNode(3);

    l1->next = l2;
    l2->next = l3;

    return l1;
}

ListNode * GetL2()
{
    ListNode *l1 = new ListNode(5);
    ListNode *l2 = new ListNode(6);
    ListNode *l3 = new ListNode(4);

    l1->next = l2;
    l2->next = l3;

    return l1;
}

void PrintNum(ListNode *node)
{
    if(node)
    {
        std::cout << node->val << "";
        PrintNum(node->next);
    }
}

ListNode * addTwoNumbers(ListNode *l1, ListNode *l2)
{
    ListNode *out = nullptr;
    ListNode *current = nullptr;
    ListNode *node1 = l1;
    ListNode *node2 = l2;
    int nextAdd = 0;
    do
    {
        int val1 = node1 == nullptr ? 0 : node1->val;
        int val2 = node2 == nullptr ? 0 : node2->val;
        int value = (val1 + val2 + nextAdd) % 10;
        nextAdd = (val1 + val2 + nextAdd) / 10;
        if (node1 || node2 || value > 0)
        {
            ListNode *next = new ListNode(value);
            if(out)
            {
                current->next = next;
                current = current->next;
            }
            else
            {
                out = next;
                current = out;
            }

            node1 = node1 == nullptr ? nullptr : node1->next;
            node2 = node2 == nullptr ? nullptr : node2->next;
        }
    }
    while(node1 || node2 || nextAdd > 0);

    return out;
}

void TestAddTwoNumbers()
{
    ListNode *l1 = GetL1();
    ListNode *l2 = GetL2();
    ListNode *sum = addTwoNumbers(l1, l2);
    PrintNum(sum);
    std::cout << std::endl;
    PrintNum(l1);
    std::cout << std::endl;
}
