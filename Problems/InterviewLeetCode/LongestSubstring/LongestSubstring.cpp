//
//  LongestSubstring.cpp
//  Yandex.Algorithm
//
//  Created by moxxim on 5/14/18.
//  Copyright © 2018 moxxim. All rights reserved.
//

/*
 Given a string, find the length of the longest substring without repeating characters.
 
 Ex.
 
 Given "abcabcbb", the answer is "abc", which the length is 3.

 Given "bbbbb", the answer is "b", with the length of 1.

 Given "pwwkew", the answer is "wke", with the length of 3. Note that the answer must be a substring, "pwke" is a subsequence and not a substring.
 */

#include "LongestSubstring.hpp"
#include <iostream>
#include <string>

// O(n)
int _lengthOfLongestSubstring(std::string s) {
    /*
    int n = s.length();
    Set<Character> set = new HashSet<>();
    int ans = 0, i = 0, j = 0;
    while (i < n && j < n) {
        // try to extend the range [i, j]
        if (!set.contains(s.charAt(j))){
            set.add(s.charAt(j++));
            ans = Math.max(ans, j - i);
        }
        else {
            set.remove(s.charAt(i++));
        }
    }
    return ans;
     */
    return 0;
}

// O(n^3)
int lengthOfLongestSubstring(std::string s)
{
    struct substring
    {
        unsigned start;
        unsigned length;
    };

    auto stringLength = s.length();
    substring longest;
    longest.start = 0;
    longest.length = 0;

    for(unsigned i = 0; i < stringLength; ++i)
    {
        unsigned lag = 1;
        for(unsigned j = i + 1; j < stringLength; ++j)
        {
            auto character = s[j];
            bool isDuplicate = false;
            for(unsigned k = i; k < j; ++k)
            {
                if(s[k] == character)
                {
                    isDuplicate = true;
                    break;
                }
            }

            if(isDuplicate)
            {
                break;
            }
            else
            {
                ++lag;
            }
        }

        if (lag > longest.length)
        {
            longest.start = i;
            longest.length = lag;
        }
    }

    return longest.length;
}

void TestLongestSubstring()
{
    std::string in = "anviaj";
    int out = lengthOfLongestSubstring(in);
    std::cout << out << std::endl;
}
