#ifndef ReverseInt_hpp
#define ReverseInt_hpp

// Reverses the integer number.
// 123 -> 321;
// -123 -> -321;
// 120 -> 21;
// 2147483647 -> 0 (overflow).
inline int ReverseInt(int x)
{
    int result = 0;

    while (x != 0)
    {
        int tail = x % 10;
        int newResult = result * 10 + tail;
        if ((newResult - tail) / 10 != result)
        {
            return 0;
        }

        result = newResult;
        x = x / 10;
    }

    return result;
}

void TestReverseInt();

#endif /* ReverseInt_hpp */
