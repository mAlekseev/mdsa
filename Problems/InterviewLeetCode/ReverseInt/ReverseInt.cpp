//
//  ReverseInt.cpp
//  Yandex.Algorithm
//
//  Created by moxxim on 5/14/18.
//  Copyright © 2018 moxxim. All rights reserved.
//

#include "ReverseInt.hpp"
#include <iostream>

void TestReverseInt()
{
    int in = ~0;
    int out = ReverseInt(in);

    std::cout << in << " -> " << out << std::endl;
}
