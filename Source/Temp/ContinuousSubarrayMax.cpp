#include <iostream>
#include <deque> 

#ifndef CoutinuousSubarraysMax_hpp
#define CoutinuousSubarraysMax_hpp

namespace mox
{
namespace alg
{
// Complexity - O(2*n).
// For every element in array there performed exectly one deque::push() and one deque::pop()
// Note: printf() is much faster than std::cout
void PrintContinuousSubarraysMax(int arr[], int n, int k)
{
    // Create a double-ended queue, that will store indexes of array elements.
    // The queue will store indexes of useful elements in every window.
    // It will maintain decreasing order of values from front to rear.
    // I.e., arr[indexes.front()] to arr[indexes.back()] are sorted in decreasing order.

    std::deque<int> indexes(k);

    for (int i = 0 ; i < n; ++i)
    {
        if (i >= k)
        {
            // The element at the front of the queue is the largest element of previous window.
            printf("%i ", arr[indexes.front()]);

            // Remove the element if it is out of this window.
            if (indexes.front() <= i - k)
            {
                indexes.pop_front();
            }
        }

        // Remove all elements smaller than the current one.
        while ((!indexes.empty()) && arr[i] >= arr[indexes.back()])
        {
            indexes.pop_back();
        }

        indexes.push_back(i);
    }

    printf("%i", arr[indexes.front()]);
}
}
}

#endif
