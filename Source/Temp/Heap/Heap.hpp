#ifndef Heap_hpp
#define Heap_hpp

namespace dst
{
	template <class T>
	class Heap final
	{

	public:

		explicit Heap(bool increasing = true);
		Heap(const Heap&) = delete;

		~Heap();

		// Gets the number of elements in the heap.
		unsigned GetSize() const { return m_size; };

		// Gets a value indicating whether the heap is empty.
		bool IsEmpty() const { return m_size == 0; }

		// Adds an element inside the heap.
		void Add(const T& value);

		// Peeks the extreme element inside the heap.
		T Peek() const;

		// Pops the extreme element inside the heap.
		T Pop();

		// Clears the heap.
		void Clear();

	private:

		struct Node final
		{
			Node(T value, Node *previous);
			~Node();

			T Value;
			Node *Previous = nullptr;
			Node *Left = nullptr;
			Node *Right = nullptr;
		};

		static Node * FindLast(Node *root, unsigned lowCount, unsigned lowMaxCount);
		static Node * FindLastSlot(Node *root, unsigned lowCount, unsigned lowMaxCount);
		static void SetBinding(Node *forParent, Node *forChild, bool left);

		Node * CreateNode(const T& value);
		void Remove(Node *node);
		bool Compare(const Node *node, const Node *target, bool forUpheap) const;
		Node * GetDownheap(const Node *node) const;
		void Upheap(Node *node);
		void Downheap(Node *node);
		void SwapPlaces(Node *node1, Node *node2);

		Node *m_root = nullptr;
		unsigned m_size = 0;
		unsigned m_lowNodesCount = 0;
		unsigned m_lowNodesMaxCount = 1;
		bool m_increasing = false;

	};

	template <class T>
	Heap<T>::Heap(bool increasing) : m_increasing(increasing)
	{
	}

	template <class T>
	Heap<T>::~Heap()
	{
		delete m_root;
	}

	template <class T>
	void Heap<T>::Add(const T& value)
	{
		auto node = CreateNode(value);

		if (m_root)
		{
			Upheap(node);
		}
		else
		{
			m_root = node;
		}

		++m_lowNodesCount;
		if (m_lowNodesCount == m_lowNodesMaxCount)
		{
			m_lowNodesCount = 0;
			m_lowNodesMaxCount *= 2;
		}

		++m_size;
	}

	template <class T>
	inline T Heap<T>::Peek() const
	{
		//assert(m_root != nullptr);

		return m_root->Value;
	}

	template <class T>
	T Heap<T>::Pop()
	{
		//assert(m_root != nullptr);

		auto lowNodesCount = m_lowNodesCount == 0 ? m_lowNodesMaxCount / 2 : m_lowNodesCount;
		auto lowNodesMaxCount = m_lowNodesCount == 0 ? m_lowNodesMaxCount / 2 : m_lowNodesMaxCount;
		auto lastNode = FindLast(m_root, lowNodesCount, lowNodesMaxCount);

		T value = m_root->Value;
		if (lastNode == m_root)
		{
			Remove(m_root);
		}
		else
		{
			auto node = m_root;

			SwapPlaces(node, lastNode);
			Remove(node);
			Downheap(lastNode);
		}

		--m_size;

		return value;
	}

	template <class T>
	inline void Heap<T>::Clear()
	{
		delete m_root;
		m_root = nullptr;

		m_lowNodesCount = 0;
		m_lowNodesMaxCount = 1;
		m_size = 0;
	}

	template <class T>
	typename Heap<T>::Node * Heap<T>::FindLast(Node *root, unsigned lowCount, unsigned lowMaxCount)
	{
		if (root)
		{
			auto lowMaxCountHalf = lowMaxCount / 2;
			if (lowCount <= lowMaxCountHalf)
			{
				if (root->Left)
				{
					return FindLast(root->Left, lowCount, lowMaxCountHalf);
				}
			}
			else
			{
				if (root->Right)
				{
					return FindLast(root->Right, lowCount - lowMaxCountHalf, lowMaxCountHalf);
				}
			}

			return root;
		}

		return nullptr;
	}

	template <class T>
	typename Heap<T>::Node * Heap<T>::FindLastSlot(Node *root, unsigned lowCount, unsigned lowMaxCount)
	{
		if (root)
		{
			auto lowMaxCountHalf = lowMaxCount / 2;
			if (lowCount < lowMaxCountHalf)
			{
				if (root->Left)
				{
					return FindLastSlot(root->Left, lowCount, lowMaxCountHalf);
				}
			}
			else
			{
				if (root->Right)
				{
					return FindLastSlot(root->Right, lowCount - lowMaxCountHalf, lowMaxCountHalf);
				}
			}

			return root;
		}

		return nullptr;
	}

	template <class T>
	void Heap<T>::SetBinding(Node *parent, Node *child, bool left)
	{
		if (child)
		{
			child->Previous = parent;
		}

		if (parent)
		{
			if (left)
			{
				parent->Left = child;
			}
			else
			{
				parent->Right = child;
			}
		}
	}

	template <class T>
	typename Heap<T>::Node * Heap<T>::CreateNode(const T& value)
	{
		auto lastSlot = FindLastSlot(m_root, m_lowNodesCount, m_lowNodesMaxCount);
		auto node = new Node(value, lastSlot);

		if (lastSlot)
		{
			if (lastSlot->Left)
			{
				lastSlot->Right = node;
			}
			else
			{
				lastSlot->Left = node;
			}
		}

		return node;
	}

	template <class T>
	void Heap<T>::Remove(Node *node)
	{
		auto previous = node->Previous;
		if (previous)
		{
			if (previous->Left == node)
			{
				previous->Left = nullptr;
			}
			else
			{
				previous->Right = nullptr;
			}
		}
		else
		{
			m_root = nullptr;
		}

		delete node;

		if (m_lowNodesCount == 0)
		{
			m_lowNodesMaxCount /= 2;
			m_lowNodesCount = m_lowNodesMaxCount - 1;
		}
		else
		{
			--m_lowNodesCount;
		}
	}

	template <class T>
	bool Heap<T>::Compare(const Node *node, const Node *target, bool forUpheap) const
	{
		if (forUpheap)
		{
			if (m_increasing)
			{
				return node->Value < target->Value;
			}
			else
			{
				return node->Value > target->Value;
			}
		}
		else
		{
			if (m_increasing)
			{
				return node->Value > target->Value;
			}
			else
			{
				return node->Value < target->Value;
			}
		}
	}

	template <class T>
	typename Heap<T>::Node * Heap<T>::GetDownheap(const Node *node) const
	{
		auto left = node->Left;
		auto right = node->Right;

		if (left && right)
		{
			bool goToLeft = Compare(node, left, false);
			bool goToRight = Compare(node, right, false);

			if (goToLeft && goToRight)
			{
				return Compare(right, left, false) ? left : right;
			}
			else if (goToLeft)
			{
				return left;
			}
			else if (goToRight)
			{
				return right;
			}
		}
		else if (left)
		{
			if (Compare(node, left, false))
			{
				return left;
			}
		}

		return nullptr;
	}

	template <class T>
	void Heap<T>::Upheap(Node *node)
	{
		auto previous = node->Previous;
		if (previous)
		{
			if (Compare(node, previous, true))
			{
				SwapPlaces(previous, node);
				Upheap(node);
			}
		}
	}

	template <class T>
	void Heap<T>::Downheap(Node *node)
	{
		auto target = GetDownheap(node);
		if (target)
		{
			SwapPlaces(node, target);
			Downheap(node);
		}
	}

	template <class T>
	void Heap<T>::SwapPlaces(Node *upNode, Node *lowNode)
	{
		if (upNode == lowNode)
		{
			return;
		}

		bool areNeighbors = upNode->Left == lowNode || upNode->Right == lowNode;
		if (areNeighbors)
		{
			auto isAtLeft = upNode->Previous && upNode->Previous->Left == upNode;
			SetBinding(upNode->Previous, lowNode, isAtLeft);

			isAtLeft = upNode->Left == lowNode;
			auto temp = isAtLeft ? lowNode->Left : lowNode->Right;
			SetBinding(lowNode, upNode, isAtLeft);
			SetBinding(upNode, temp, isAtLeft);

			temp = isAtLeft ? lowNode->Right : lowNode->Left;
			SetBinding(lowNode, isAtLeft ? upNode->Right : upNode->Left, !isAtLeft);
			SetBinding(upNode, temp, !isAtLeft);
		}
		else
		{
			auto isUpperAtLeft = upNode->Previous && upNode->Previous->Left == upNode;
			auto isLowerAtLeft = lowNode->Previous && lowNode->Previous->Left == lowNode;
			auto temp = upNode->Previous;

			SetBinding(lowNode->Previous, upNode, isLowerAtLeft);
			SetBinding(temp, lowNode, isUpperAtLeft);

			temp = upNode->Right;
			SetBinding(upNode, lowNode->Right, false);
			SetBinding(lowNode, temp, false);

			temp = upNode->Left;
			SetBinding(upNode, lowNode->Left, true);
			SetBinding(lowNode, temp, true);
		}

		if (m_root == upNode)
		{
			m_root = lowNode;
		}
		else if (m_root == lowNode)
		{
			m_root = upNode;
		}
	}

	template <class T>
	Heap<T>::Node::Node(T value, Node *previous) :
		Value(value),
		Previous(previous)
	{
	}

	template <class T>
	Heap<T>::Node::~Node()
	{
		delete Left;
		delete Right;
	}
}

#endif // Heap_hpp
