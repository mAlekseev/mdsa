/*
 A student signed up for N workshops and wants to
 attend the maximum number of workshops where no two workshops overlap.
*/


#ifndef DisjunctiveSegmentsMaxCount_h
#define DisjunctiveSegmentsMaxCount_h

namespace mox
{
namespace alg
{

template <typename T>
struct Range
{
    T A;
    T B;
};

template <typename T>
int GetMaxDisjunctiveSegmentsCount(std::vector<Range<T>>& segments)
{
    auto segmentsCount = segments.size();
    auto end = 0U;
    auto disjunctiveSegmentsCount = 0U;

    auto compare = [](const Range<T>& s1, const Range<T>& s2)
    {
        if(s1.A < s2.A) return true;
        else if(s1.A == s2.A) return s1.B > s2.B;
        else return false;
    };

    std::sort(segments.begin(), segments.end(), compare);
    for(auto i = 0U; i < segmentsCount; ++i)
    {
        auto segment = segments[i];
        if(segment.B <= end)
        {
            end = segment.B;
        }
        else if (segment.A >= end)
        {
            end = segment.B;
            ++disjunctiveSegmentsCount;
        }
    }

    return disjunctiveSegmentsCount;
}
}
}

#endif /* DisjunctiveSegmentsMaxCount_h */
