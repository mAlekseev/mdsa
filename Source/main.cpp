//
//  main.cpp
//  Mdsa
//
//  Created by moxxim on 4/3/20.
//  Copyright © 2020 moxxim. All rights reserved.
//

#include <Mdsa/Algorithms/Algorithms.hpp>
#include <Mdsa/Algorithms/Sorting.hpp>
#include <Mdsa/Algorithms/Test/TestRandom.hpp>
#include <Mdsa/Algorithms/Test/TestSorting.hpp>
#include <Mdsa/TestUtils/TestUtils.hpp>

#include <iostream>
#include <vector>
#include <random>

int main(int argc, const char * argv[])
{
    Mdsa::Test::TestSorting();
}
