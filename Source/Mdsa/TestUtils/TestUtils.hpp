//
//  TestUtils.hpp
//  Mdsa
//
//  Created by moxxim on 4/5/20.
//  Copyright © 2020 moxxim. All rights reserved.
//

#ifndef TestUtils_hpp
#define TestUtils_hpp

#include <string>
#include <sstream>
#include <vector>

namespace Mdsa
{
    namespace Test
    {
        // TODO: make template through iterators.
        template <typename TInt>
        std::string ArrayToString(const std::vector<TInt> &arr);
    }
}

namespace Mdsa
{
    namespace Test
    {
        template <typename TInt>
        std::string ArrayToString(const std::vector<TInt> &arr)
        {
            std::stringstream stream;

            stream << "{";
            uint32_t size = static_cast<uint32_t>(arr.size());
            for(uint32_t i = 0; i < size; ++i)
            {
                stream << arr[i];
                if(i < size - 1)
                {
                    stream << ",";
                }
            }

            stream << "}";

            return stream.str();
        }
    }
}

#endif /* TestUtils_hpp */
