#include "LongestCommonSubsequence.hpp"

namespace Mdsa
{
    unsigned CalculateLcsLen(std::string str1, std::string str2)
    {
        size_t len1 = str1.length();
        size_t len2 = str2.length();

        std::vector<unsigned> longest;
        longest.resize(len1 * len2);
        std::fill_n(longest.begin(), len1 * len2, 0);

        for(size_t i = 0; i < len2; ++i)
        {
            size_t index2 = (len2 - 1) - i;

            for(size_t j = 0; j < len1; ++j)
            {
                size_t index1 = (len1 - 1) - j;

                unsigned prevIi = index2 < (len2 - 1) ? longest[len1 * (index2 + 1) + index1] : 0;
                unsigned prevJj = index1 < (len1 - 1) ? longest[len1 * index2 + (index1 + 1)] : 0;
                unsigned prevDiag = (index2 < (len2 - 1)) && (index1 < (len1 - 1))
                    ? longest[len1 * (index2 + 1) + (index1 + 1)]
                    : 0;

                longest[len1 * index2 + index1] = (str1[index1] == str2[index2])
                    ? std::max(prevDiag + 1, prevJj)
                    : std::max(prevIi, prevJj);
            }
        }

        return longest[0];
    }
}
