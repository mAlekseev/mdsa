//
//  Algorithms.hpp
//  mdsa
//
//  Created by moxxim on 1/12/20.
//  Copyright © 2020 moxxim. All rights reserved.
//

#ifndef Algorithms_hpp
#define Algorithms_hpp

#include <utility>
#include <vector>
#include <set>

namespace Mdsa
{
    /*
        \brief Randomly shuffles an array.
        \complexity O(n)
     */
    void Shuffle(std::vector<int> &arr);

    /*
        \brief Generates random sample of m non-repeting integers from the range [0, n].
        \complexity O(n).
    */
    std::set<int> RandomSample(int m, int n);

    // TODO: Implement without extra memory allocation. Implement for various containers.
    /*
        \brief Generates random sample of m non-repeting elements of specified array.
        \complexity O(n), where n is the length of array.
     */
    std::vector<int> RandomSample(const std::vector<int> &arr, size_t m);

    /*
        \brief Finds pair of indexes (i, j) such that: container[i] + container[j] = value.
        \complexity O(n)
        \returns Returns pair (n, n) if there are not 2 elements x and y in container such that: x + y = value.
     */
    std::pair<size_t, size_t> FindTerms(const std::vector<int> &arr, int value);

    /*
        \brief Finds the number of inversions in specified array.
        \complexity O(n*lg(n))
        \note A pair of indexes (i, j) is called an inversion if i < j, but arr[i] > arr[j]
        \returns Number of inversions in specified container.
     */
    size_t CountInversions(const std::vector<int> &arr);

    /*
        \brief Finds subarray with maximal sum.
        \note Complexity is O(n)
        \returns Pair of indexes in input array, specifying maximal continuent subarray in form of semiinterval [a, b).
    */
    std::pair<size_t, size_t> FindMaxSubarray(const std::vector<int> &arr);
}

namespace Mdsa
{
    namespace Suboptimal
    {
        /*
            \brief Finds subarray with maximal sum.
            \complexity O(n*n)
            \returns Pair of indexes in input array, specifying maximal continuent subarray in form of semiinterval [a, b).
        */
        std::pair<size_t, size_t> FindMaxSubarrayBruteForce(const std::vector<int> &arr);

        /*
            \brief Finds subarray with maximal sum.
            \complexity O(n*lg(n))
            \returns Pair of indexes in input array, specifying maximal continuent subarray in form of semiinterval [a, b).
        */
        std::pair<size_t, size_t> FindMaxSubarrayDivide(const std::vector<int> &arr);
    }
}

#endif /* Algorithms_hpp */
