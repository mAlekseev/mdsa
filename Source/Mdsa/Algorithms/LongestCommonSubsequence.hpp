//
//  LongestCommonSubsequence.hpp
//  mdsa
//
//  Created by moxxim on 1/4/20.
//  Copyright © 2020 moxxim. All rights reserved.
//

#ifndef LongestCommonSubsequence_hpp
#define LongestCommonSubsequence_hpp

#include <string>
#include <vector>
#include <algorithm>

namespace Mdsa
{
    unsigned CalculateLcsLen(std::string str1, std::string str2);
    template <typename It1, typename It2>
    unsigned CalculateLcsLen(std::string str1, std::string str2);
}

namespace Mdsa
{
    template <typename RandomIt1, typename RandomIt2>
    unsigned CalculateLcsLen(
                           RandomIt1 begin1,
                           RandomIt1 end1,
                           RandomIt2 begin2,
                           RandomIt2 end2)
    {
        using Diff1_t = typename std::iterator_traits<RandomIt1>::difference_type;
        using Diff2_t = typename std::iterator_traits<RandomIt2>::difference_type;

        Diff1_t len1 = end1 - begin1;
        Diff2_t len2 = end2 - begin2;

        std::vector<unsigned> longest;
        longest.resize(len1 * len2);
        std::fill_n(longest.begin(), len1 * len2, 0);

        if((end1 == begin1) || (end2 == begin2))
        {
            return 0;
        }

        for(RandomIt1 curr2 = end2 - 1; ; --curr2)
        {
            Diff1_t index2 = curr2 - begin2;

            for(RandomIt2 curr1 = end1 - 1; ; --curr1)
            {
                Diff2_t index1 = curr1 - begin1;

                unsigned prevVal1 = (curr2 != end2) ? longest[len1 * (index2 + 1) + index1] : 0;
                unsigned prevVal2 = (curr1 != end1) ? longest[len1 * index2 + (index1 + 1)] : 0;
                unsigned prevDiag = ((curr1 != end1) && (curr2 != end2))
                    ? longest[len1 * (index2 + 1) + (index1 + 1)]
                    : 0;

                longest[(len1 * index2) + index1] = ((*curr2) == (*curr1))
                    ? std::max(prevDiag + 1, prevVal2)
                    : std::max(prevVal1, prevVal2);

                if(curr1 == begin1)
                {
                    break;
                }
            }

            if(curr2 == begin2)
            {
                break;
            }
        }

        return longest[0];
    }
}

#endif /* LongestCommonSubsequence_hpp */
