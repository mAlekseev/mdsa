//
//  Sorting.cpp
//  mdsa
//
//  Created by moxxim on 1/6/20.
//  Copyright © 2020 moxxim. All rights reserved.
//

#include <Mdsa/Algorithms/Sorting.hpp>

#include <Mdsa/DataStructures/Heap.hpp>

#include <utility>
#include <random>

namespace Mdsa
{
    namespace SSorting
    {
        namespace Insert
        {
            void Sort(std::vector<int> &arr, bool isAccending)
            {
                auto isSecondBefore = [isAccending](int first, int second) -> bool
                {
                    if(isAccending)
                    {
                        return second < first;
                    }
                    else
                    {
                        return first < second;
                    }
                };

                size_t sz = arr.size();

                for(size_t i = 1; i < sz; ++i)
                {
                    int value = arr[i];

                    // Loop invariant: arr[0, ..., i - 1] is always sorted.

                    size_t j = i - 1;
                    while(isSecondBefore(arr[j], value))
                    {
                        arr[j + 1] = arr[j];
                        if(j == 0)
                        {
                            break;
                        }

                        --j;
                    }

                    if(j == 0 && isSecondBefore(arr[0], value))
                    {
                        arr[0] = value;
                    }
                    else
                    {
                        arr[j + 1] = value;
                    }
                }
            }
        }

        namespace Select
        {
            void Sort(std::vector<int> &arr, bool isAccending)
            {
                if(arr.empty())
                {
                    return;
                }

                auto needSwap = [isAccending](int first, int second)
                {
                    if(isAccending)
                    {
                        return first > second;
                    }
                    else
                    {
                        return first < second;
                    }
                };

                // Loop invariant: arr[0, ..., i - 1] is always sorted.
                //                 For each j in [0, i - 1] and each k in [i, n - 1]: arr[j] < arr[k].
                for(int i = 0; i < arr.size() - 1; ++i)
                {
                    int extreme = i;
                    for(int j = i + 1; j < arr.size(); ++j)
                    {
                        if(needSwap(arr[extreme], arr[j]))
                        {
                            extreme = j;
                        }
                    }

                    if(i != extreme)
                    {
                        std::swap(arr[i], arr[extreme]);
                    }
                }
            }
        }

        namespace Merge
        {
            void Merge(
                        std::vector<int> &arr,
                        size_t begin,
                        size_t between,
                        size_t end,
                        bool isAccending,
                        std::vector<int> &buffer)
            {
                auto needTakeLeft = [isAccending](int lhs, int rhs)
                {
                    if(isAccending)
                    {
                        return lhs < rhs;
                    }
                    else
                    {
                        return lhs > rhs;
                    }
                };

                size_t left = begin;
                size_t right = between;

                // Loop invariant: buffer[begin, ..., i) is always sorted.
                //                 arr[left] - extreme in arr[left, between)
                //                 arr[right] - extreme in arr[right, end).
                for(size_t i = begin; i < end; ++i)
                {
                    bool takeLeft = true;
                    if(left == between)
                    {
                        takeLeft = false;
                    }
                    else if(right == end)
                    {
                        takeLeft = true;
                    }
                    else
                    {
                        takeLeft = needTakeLeft(arr[left], arr[right]);
                    }

                    if(takeLeft)
                    {
                        buffer[i] = arr[left];
                        ++left;
                    }
                    else
                    {
                        buffer[i] = arr[right];
                        ++right;
                    }
                }

                // arr[begin, i) is sorted.
                for(size_t i = begin; i < end; ++i)
                {
                    arr[i] = buffer[i];
                }
            }

            void SortRange(
                        std::vector<int> &arr,
                        size_t begin,
                        size_t end,
                        bool isAccending,
                        std::vector<int> &buffer)
            {
                if((begin + 1) < end)
                {
                    size_t between = (end + begin) / 2;
                    SortRange(arr, begin, between, isAccending, buffer);
                    SortRange(arr, between, end, isAccending, buffer);
                    Merge(arr, begin, between, end, isAccending, buffer);
                }
            }

            void Sort(std::vector<int> &arr, bool isAccending)
            {
                std::vector<int> buffer;
                buffer.reserve(arr.size());
                SortRange(arr, 0, arr.size(), isAccending, buffer);
            }
        }

        namespace Bubble
        {
            void Sort(std::vector<int> &arr, bool isAccending)
            {
                auto needSwap = [isAccending](int first, int second)
                {
                    if(isAccending)
                    {
                        return first > second;
                    }
                    else
                    {
                        return first < second;
                    }
                };

                for(size_t i = 0; i < arr.size(); ++i)
                {
                    for(size_t j = arr.size() - 1; j > i; --j)
                    {
                        if(needSwap(arr[j - 1], arr[j]))
                        {
                            std::swap(arr[j - 1], arr[j]);
                        }
                    }
                }
            }
        }

        namespace Heap
        {
            void Sort(std::vector<int> &arr, bool isAccending)
            {
                HeapType heapType = isAccending ? HeapType::Max : HeapType::Min;
                MakeHeap(heapType, arr);

                size_t heapSize = arr.size();
                while(heapSize > 1)
                {
                    --heapSize;
                    std::swap(arr[0], arr[heapSize]);
                    Heapify(heapType, arr, heapSize, 0);
                }
            }
        }

        namespace Quick
        {
            namespace Hoare
            {
                size_t MakePartition(std::vector<int> &arr, size_t from, size_t to, bool isAccending)
                {
                    int pivotValue = arr[from];
                    size_t right = to - 1;

                    auto isAfterPivot = [isAccending, pivotValue](int value)
                    {
                        return isAccending ? (value >= pivotValue) : (value <= pivotValue);
                    };

                    for (size_t left = from; left < right; ++left)
                    {
                        if (isAfterPivot(arr[left]))
                        {
                            while (isAfterPivot(arr[right]) && left < right)
                            {
                                --right;
                            }

                            if (left < right)
                            {
                                std::swap(arr[left], arr[right]);
                            }
                        }
                    }

                    return (right == from) ? (from + 1) : right;
                }

                void Sort(std::vector<int> &arr, size_t from, size_t to, bool isAccending)
                {
                    if((to - from) > 1)
                    {
                        size_t partition = MakePartition(arr, from, to, isAccending);
                        Sort(arr, from, partition, isAccending);
                        Sort(arr, partition, to, isAccending);
                    }
                }

                void Sort(std::vector<int> &arr, bool isAccending)
                {
                    Sort(arr, 0, arr.size(), isAccending);
                }
            }

            size_t MakePartition(std::vector<int> &arr, size_t from, size_t to, bool isAccending)
            {
                auto needSwap = [isAccending](int value, int pivot)
                {
                    return isAccending ? (value < pivot) : (value > pivot);
                };

                std::random_device device;
                std::mt19937 generator(device());
                std::uniform_int_distribution<size_t> distribution{ from, to - 1 };
                size_t pivotIndex = distribution(generator);
                std::swap(arr[pivotIndex], arr[to - 1]);

                int pivotValue = arr[to - 1];

                /*
                 Complexity: O(n)
                 Loop invariant: before each iteraion next statements are true:
                    1) For each a in arr[from, out]: a <= pivotValue;
                    2) For each a in arr[out + 1, i - 1]: a > pivotValue;
                 */

                size_t partition = from;
                for(size_t i = from; i < (to - 1); ++i)
                {
                    if(needSwap(arr[i], pivotValue))
                    {
                        std::swap(arr[i], arr[partition]);
                        ++partition;
                    }
                }

                /*
                 Result: put a pivot value inbetween to ranges and return its index.
                 */

                std::swap(arr[partition], arr[to - 1]);
                return partition;
            }

            void Sort(std::vector<int> &arr, size_t from, size_t to, size_t threshold, bool isAccending)
           {
                if((to - from) > threshold)
                {
                    size_t partition = MakePartition(arr, from, to, isAccending);
                    Sort(arr, from, partition, threshold, isAccending);
                    Sort(arr, partition + 1, to, threshold, isAccending);
                }
            }

            void Sort(std::vector<int> &arr, bool isAccending)
            {
                Sort(arr, 0, arr.size(), 1, isAccending);
            }
        }
    }

    void Sort(eSortMethod sortMethod, std::vector<int> &arr, bool isAccending /* = true */)
    {
        // TODO: Get rid of extra invokation. Implement compile-time resolve of method selection.
        if(sortMethod == eSortMethod::Insertion)
        {
            SSorting::Insert::Sort(arr, isAccending);
        }
        else if(sortMethod == eSortMethod::Selection)
        {
            SSorting::Select::Sort(arr, isAccending);
        }
        else if(sortMethod == eSortMethod::Merge)
        {
            SSorting::Merge::Sort(arr, isAccending);
        }
        else if(sortMethod == eSortMethod::Bubble)
        {
            SSorting::Bubble::Sort(arr, isAccending);
        }
        else if(sortMethod == eSortMethod::Heap)
        {
            SSorting::Heap::Sort(arr, isAccending);
        }
        else if(sortMethod == eSortMethod::Quick)
        {
            SSorting::Quick::Sort(arr, isAccending);
        }
    }

    void Sort(std::vector<int> &arr, bool isAccending /* = true */)
    {
        // TODO: get rid of extra invokation.
        Sort(Mdsa::eSortMethod::Quick, arr, isAccending);
    }

    void SrotQuickWithInsert(std::vector<int> &arr, size_t threshold, bool isAccending /* = true */)
    {
        SSorting::Quick::Sort(arr, 0, arr.size(), threshold, isAccending);
        SSorting::Insert::Sort(arr, isAccending);
    }

    std::vector<unsigned> SortCounting(const std::vector<unsigned> &arr,  unsigned valueMax, bool accending /*= true*/)
    {
        if(arr.size() < 2)
        {
            return arr;
        }

        std::vector<unsigned> out;
        out.resize(arr.size());

        std::vector<unsigned> count;
        count.resize(valueMax + 1);
        std::fill(count.begin(), count.end(), 0);

        // Array "count" for each index holds number of elements of source array "arr" equal to the index.
        std::for_each(arr.begin(), arr.end(), [&count](unsigned value) { count[value]++; });

        // Array "count" for each index holds number of elements of source array "arr" which are less or equal to the index.
        for(unsigned i = 1; i < count.size(); ++i)
        {
            count[i] += count[i - 1];
        }

        // Array "out" now represents sorted elements of source array "arr".
        if(accending)
        {
            // Note: backward iteration is crucial for sorting to be stable.
            size_t i = arr.size();
            do
            {
                --i;
                unsigned value = arr[i];
                out[count[value] - 1] = value;
                count[value]--;
            }
            while(i > 0);
        }
        else
        {
            // Note: forward iteration is crucial for sorting to be stable.
            size_t length = arr.size();
            for(unsigned i = 0; i < length; ++i)
            {
                unsigned value = arr[i];
                size_t index = length - count[value];
                out[index] = value;
                count[value]--;
            }
        }

        return out;
    }
}
