//
//  TestSorting.hpp
//  Mdsa
//
//  Created by moxxim on 4/5/20.
//  Copyright © 2020 moxxim. All rights reserved.
//

#ifndef TestSorting_hpp
#define TestSorting_hpp

#include <stdio.h>

namespace Mdsa
{
    namespace Test
    {
        void TestSorting();
    }
}

#endif /* TestSorting_hpp */
