//
//  TestRandom.cpp
//  Mdsa
//
//  Created by moxxim on 4/5/20.
//  Copyright © 2020 moxxim. All rights reserved.
//

#include <Mdsa/Algorithms/Test/TestRandom.hpp>

#include <Mdsa/Algorithms/Algorithms.hpp>
#include <Mdsa/TestUtils/TestUtils.hpp>
#include <iostream>
#include <map>
#include <random>
#include <vector>

namespace STestRandom
{
    void PrintDistribution(const std::map<std::vector<int>, int> &distribution)
    {
        for(const auto &pair : distribution)
        {
            const std::vector<int> &permutation = pair.first;
            int count = pair.second;
            std::cout << Mdsa::Test::ArrayToString(permutation) << ":\t " << count << "\n";
        }
    }

    namespace Shuffle
    {
        using ShuffleFunction = std::function<void(std::vector<int>&)>;

        void NaiveShuffle(std::vector<int> &arr)
        {
            std::random_device device;
            std::mt19937 generator(device());
            std::uniform_int_distribution<uint32_t> distribution{0, static_cast<uint32_t>(arr.size()) - 1};
            for(uint32_t i = 0; i < arr.size(); ++i)
            {
                uint32_t randomIndex = distribution(generator);
                std::swap(arr[i], arr[randomIndex]);
            }
        }

        std::map<std::vector<int>, int> EvaluateShuffleDistribution(
                                                        std::vector<int> arr,
                                                        uint32_t shufflesCount,
                                                        const ShuffleFunction &shuffle)
        {
            std::vector<int> initial = arr;
            std::map<std::vector<int>, int> distribution;

            for(uint32_t i = 0; i < shufflesCount; ++i)
            {
                shuffle(arr);
                distribution[arr]++;
                arr = initial;
            }

            return distribution;
        }

        void TestShuffle(std::vector<int> arr, uint32_t shufflesCount, const ShuffleFunction &shuffle)
        {
            std::map<std::vector<int>, int> distribution = EvaluateShuffleDistribution(arr, shufflesCount, shuffle);
            PrintDistribution(distribution);
        }
    }

    namespace Sample
    {
        std::map<std::vector<int>, int> EvaluateSamplesDistribution(int m, int n, uint32_t testsCount)
        {
            std::map<std::vector<int>, int> distribution;

            for(uint32_t i = 0; i < testsCount; ++i)
            {
                std::set<int> sampleSet = Mdsa::RandomSample(m, n);
                std::vector<int> sample{
                    std::move_iterator<std::set<int>::iterator>(sampleSet.begin()),
                    std::move_iterator<std::set<int>::iterator>(sampleSet.end())};

                distribution[sample]++;
            }

            return distribution;
        }

        void TestRandomSample(int m, int n, uint32_t testsCount)
        {
            std::map<std::vector<int>, int> distribution = EvaluateSamplesDistribution(m, n, testsCount);
            PrintDistribution(distribution);
        }
    }
}

namespace Mdsa
{
    namespace Test
    {
        void TestArrayShuffleDistribution()
        {
            const uint32_t count = 600'000;
            std::vector<int> arr {1, 2, 3};

            std::cout << "\n---------- START TEST: Shuffle ----------\n";

            std::cout << "- Shuffle uniform:\n\n";
            STestRandom::Shuffle::TestShuffle(arr, count, Mdsa::Shuffle);

            std::cout << "\n";
            std::cout << "- Shuffle naive (non uniformly):\n\n";
            STestRandom::Shuffle::TestShuffle(arr, count, STestRandom::Shuffle::NaiveShuffle);

            std::cout << "\n---------- FINISH TEST: Shuffle ----------\n";
        }

        void TestRandomSampleDistribution()
        {
            const uint32_t count = 600'000;
            const int m = 3;
            const int n = 4;

            std::cout << "\n---------- START TEST: Random Sample ----------\n\n";

            STestRandom::Sample::TestRandomSample(m, n, count);

            std::cout << "\n---------- FINISH TEST: Random Sample ----------\n";

        }
    }
}
