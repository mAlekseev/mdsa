//
//  TestSorting.cpp
//  Mdsa
//
//  Created by moxxim on 4/5/20.
//  Copyright © 2020 moxxim. All rights reserved.
//

#include <Mdsa/Algorithms/Test/TestSorting.hpp>

#include <Mdsa/Algorithms/Sorting.hpp>
#include <Mdsa/TestUtils/TestUtils.hpp>

#include <functional>
#include <iostream>
#include <sstream>
#include <vector>

namespace Mdsa
{
    namespace Test
    {
        namespace STestSorting
        {
            constexpr unsigned kNonNegativeValueMax = 21;

            template <typename TInt>
            using GetSortedFunction = std::function<std::vector<TInt>(const std::vector<TInt>&, bool)>;
            using Sort = std::function<void(std::vector<int>&, bool)>;

            std::vector<int> GetSorted(const std::vector<int> &arr, bool accending, const Sort &sort)
            {
                std::vector<int> out = arr;
                sort(out, accending);
                return out;
            };

            std::vector<std::vector<unsigned>> GetArraysNonNegative()
            {
                return
                {
                    {13, 19, 9, 5, 12, 8, 7, 4, 21, 2, 6, 11},
                    {},
                    {2},
                    {0, 1},
                    {1, 2, 3, 4, 5, 6, 7, 8},
                    {8, 7, 6, 5, 4, 3, 2, 1},
                    {1, 2, 1, 4, 1, 3, 2, 3, 9},
                    {1, 1, 1, 1, 1, 1, 1, 1, 1}
                };
            }

            std::vector<std::vector<int>> GetArrays()
            {
                return
                {
                    {13, 19, 9, 5, 12, 8, 7, 4, 21, 2, 6, 11},
                    {},
                    {2},
                    {0, 1},
                    {1, 2, 3, 4, 5, 6, 7, 8},
                    {8, 7, 6, 5, 4, 3, 2, 1},
                    {1, 2, 1, 4, 1, 3, 2, 3, 9},
                    {1, 1, 1, 1, 1, 1, 1, 1, 1},
                    {-2},
                    {-1, 1},
                    {100, 7, 0, -10, 20, 7, 100, 8, 0, -1000}
                };
            }

            template <typename TInt>
            bool IsSorted(const std::vector<TInt> &arr, bool accending)
            {
                auto isSorted = [accending](TInt first, TInt second)
                {
                    return accending ? (first <= second) : (first >= second);
                };

                for(size_t i = 1; i < arr.size(); ++i)
                {
                    if(!isSorted(arr[i - 1], arr[i]))
                    {
                        return false;
                    }
                }

                return true;
            }

            const char *GetMethodName(eSortMethod method)
            {
                switch(method)
                {
                    case eSortMethod::Insertion:
                        return "Insertion";

                    case eSortMethod::Selection:
                        return "Selection";

                    case eSortMethod::Merge:
                        return "Merge";

                    case eSortMethod::Bubble:
                        return "Bubble";

                    case eSortMethod::Heap:
                        return "Heap";

                    case eSortMethod::Quick:
                        return "Quick";

                    default:
                        throw "Unknown sorting method";
                }
            }

            template <typename TInt>
            void PrintArraySorting(
                            std::vector<TInt> inArray,
                            GetSortedFunction<TInt> sort,
                            bool accending)
            {
                std::stringstream strStream;
                strStream << ArrayToString(inArray);
                strStream << " = " << (accending ? "acc" : "dec") << " => ";

                std::vector<TInt> sortedArray = sort(inArray, accending);
                strStream << ArrayToString(sortedArray);

                strStream << "\n";
                std::cout << (IsSorted(sortedArray, accending) ? "   " : "!! ") << strStream.str();
            }

            template <typename TInt>
            void RunTest(
                        const std::vector<std::vector<TInt>> &arrays,
                        GetSortedFunction<TInt> sort,
                        std::string title)
            {
                std::cout << "\nTest " << "\"" << title << "\"" << "\n";
                for(std::vector<TInt> testArray : arrays)
                {
                    PrintArraySorting(testArray, sort, true);
                    PrintArraySorting(testArray, sort, false);
                }
            }
        }

        void TestSorting()
        {
            std::vector<std::vector<unsigned>> arraysNonNegative = STestSorting::GetArraysNonNegative();
            std::vector<std::vector<int>> arrays = STestSorting::GetArrays();

            using namespace STestSorting;
            using namespace std::placeholders;

            std::cout << "\n---------- START TEST: Sorting ----------\n";

            using SortWith = void(eSortMethod sortMethod, std::vector<int> &arr, bool isAccending);

            auto sortWith = std::bind(static_cast<SortWith*>(Mdsa::Sort), eSortMethod::Insertion, _1, _2);
            STestSorting::RunTest<int>(
                        arrays,
                        [sortWith](const std::vector<int> &a, bool accend) { return GetSorted(a, accend, sortWith); },
                        GetMethodName(eSortMethod::Insertion));

            sortWith = std::bind(static_cast<SortWith*>(Mdsa::Sort), eSortMethod::Selection, _1, _2);
            STestSorting::RunTest<int>(
                        arrays,
                        [sortWith](const std::vector<int> &a, bool accend) { return GetSorted(a, accend, sortWith); },
                        GetMethodName(eSortMethod::Selection));

            sortWith = std::bind(static_cast<SortWith*>(Mdsa::Sort), eSortMethod::Bubble, _1, _2);
            STestSorting::RunTest<int>(
                        arrays,
                        [sortWith](const std::vector<int> &a, bool accend) { return GetSorted(a, accend, sortWith); },
                        GetMethodName(eSortMethod::Bubble));

            sortWith = std::bind(static_cast<SortWith*>(Mdsa::Sort), eSortMethod::Merge, _1, _2);
            STestSorting::RunTest<int>(
                        arrays,
                        [sortWith](const std::vector<int> &a, bool accend) { return GetSorted(a, accend, sortWith); },
                        GetMethodName(eSortMethod::Merge));

            sortWith = std::bind(static_cast<SortWith*>(Mdsa::Sort), eSortMethod::Heap, _1, _2);
            STestSorting::RunTest<int>(
                        arrays,
                        [sortWith](const std::vector<int> &a, bool accend) { return GetSorted(a, accend, sortWith); },
                        GetMethodName(eSortMethod::Heap));

            sortWith = std::bind(static_cast<SortWith*>(Mdsa::Sort), eSortMethod::Quick, _1, _2);
            STestSorting::RunTest<int>(
                        arrays,
                        [sortWith](const std::vector<int> &a, bool accend) { return GetSorted(a, accend, sortWith); },
                        GetMethodName(eSortMethod::Quick));

            auto sortQuickWithInsert = [](const std::vector<int> &a, bool accend)
            {
                return GetSorted(a, accend, std::bind(&Mdsa::SrotQuickWithInsert, _1, 3, _2));
            };

            STestSorting::RunTest<int>(arrays, sortQuickWithInsert, "Quick With Insert 3");

            auto sortCounting = [](const std::vector<unsigned> &a, bool accend)
            {
                return Mdsa::SortCounting(a, STestSorting::kNonNegativeValueMax, accend);
            };

            STestSorting::RunTest<unsigned>(arraysNonNegative, sortCounting, "Counting Sort");

            std::cout << "\n---------- FINISH TEST: Sorting ----------\n";
        }
    }
}
