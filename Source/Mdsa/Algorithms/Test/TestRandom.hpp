//
//  TestRandom.hpp
//  Mdsa
//
//  Created by moxxim on 4/5/20.
//  Copyright © 2020 moxxim. All rights reserved.
//

#ifndef TestRandom_hpp
#define TestRandom_hpp

#include <stdio.h>

namespace Mdsa
{
    namespace Test
    {
        void TestArrayShuffleDistribution();
        void TestRandomSampleDistribution();
    }
}

#endif /* TestRandom_hpp */
