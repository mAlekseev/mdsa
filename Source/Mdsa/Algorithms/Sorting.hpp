//
//  Sorting.hpp
//  mdsa
//
//  Created by moxxim on 1/6/20.
//  Copyright © 2020 moxxim. All rights reserved.
//

#ifndef Sorting_hpp
#define Sorting_hpp

#include <vector>

namespace Mdsa
{
    // TODO: make compile-time method resolving.
    enum class eSortMethod
    {
        // Complexity: O(n*n)
        Insertion = 1,

        // Complexity: O(n*n)
        Selection = 2,

        // Complexity: O(n*n)
        Bubble = 3,

        // Complexity: O(n*lg(n))
        Merge = 4,

        // Complexity: O(n*lg(n))
        Heap = 5,

        // Complexity: O(n*lg(n))
        Quick = 6,
    };

    void Sort(eSortMethod sortMethod, std::vector<int> &arr, bool isAccending = true);
    void Sort(std::vector<int> &arr, bool isAccending = true);

    // Complexity: O(n*threshold + lg(n/threshold))
    void SrotQuickWithInsert(std::vector<int> &arr, size_t threshold, bool isAccending = true);

    // Complexity: O(2(n + valueMax)). Extra space: O(valueMax). Sort is stable.
    std::vector<unsigned> SortCounting(const std::vector<unsigned> &arr, unsigned valueMax, bool accending = true);
}

#endif /* Sorting_hpp */
