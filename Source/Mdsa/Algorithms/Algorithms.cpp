//
//  Algorithms.cpp
//  mdsa
//
//  Created by moxxim on 1/12/20.
//  Copyright © 2020 moxxim. All rights reserved.
//

#include <Mdsa/Algorithms/Algorithms.hpp>
//#include "Algorithms.hpp"
#include "Sorting.hpp"
#include <random>

namespace Mdsa
{
    namespace SAlgorithms
    {
        int SumRange(const std::vector<int> &arr, size_t begin, size_t end)
        {
            int sum = 0;

            for(size_t i = begin; i < end; ++i)
            {
                sum += arr[i];
            }

            return sum;
        }

        namespace Inversions
        {
            size_t CountInterrangeInversions(
                                             std::vector<int> &arr,
                                             size_t begin,
                                             size_t between,
                                             size_t end,
                                             std::vector<int> &buffer)
            {
                size_t inversionsCount = 0;
                size_t left = begin;
                size_t right = between;

                /*
                 Loop invariant: buffer[begin, ..., i) is always sorted.
                                 arr[left] - extreme in arr[left, between)
                                 arr[right] - extreme in arr[right, end).
                */
                for(size_t i = begin; i < end; ++i)
                {
                    if(left == between)
                    {
                        buffer[i] = arr[right];
                        ++right;
                    }
                    else if(right == end)
                    {
                        buffer[i] = arr[left];
                        ++left;
                    }
                    else if(arr[right] < arr[left])
                    {
                        inversionsCount += (between - left);
                        buffer[i] = arr[right];
                        ++right;
                    }
                    else
                    {
                        buffer[i] = arr[left];
                        ++left;
                    }
                }

                // arr[begin, i) is sorted.
                for(size_t i = begin; i < end; ++i)
                {
                    arr[i] = buffer[i];
                }

                return inversionsCount;
            }

            size_t CountInversions(std::vector<int> &arr, size_t begin, size_t end, std::vector<int> &buffer)
            {
                if(end - begin > 1)
                {
                    size_t between = (end + begin) / 2;
                    size_t lowerInversions = CountInversions(arr, begin, between, buffer);
                    size_t upperInversions = CountInversions(arr, between, end, buffer);
                    size_t interrangeInversions = CountInterrangeInversions(arr, begin, between, end, buffer);

                    return lowerInversions + upperInversions + interrangeInversions;
                }

                return 0;
            }
        }
    }

    void Shuffle(std::vector<int> &arr)
    {
        std::random_device device;
        std::mt19937 generator(device());

        /*
         Loop invariant:
            before each i-iteration probability that subarray arr[0, i-1] has certain permutation is (n - (i-1))!/n!
            (i.e. (i-1)-permutations are uniforml y distributed).
         */
        for(uint32_t i = 0; i < arr.size() - 1; ++i)
        {
            std::uniform_int_distribution<uint32_t> distribution{i, static_cast<uint32_t>(arr.size()) - 1};
            uint32_t randomIndex = distribution(generator);
            std::swap(arr[i], arr[randomIndex]);
        }

        /*
         Result permutation is uniformly distributed.
            Proof:
            X - random event, stating that during i iterations we get i-permutation (a[0],..., a[i-1], a[i]):
            A1 - random event, stating that during first (i-1) iterations we get (i-1)-permutation (a[0],...,a[i-1])
                P(A1) = (n - (i-1))!/n! (as stated by loop invariant).
            A2 - random event, stating that during i-iteration we get certain element a[i].
            P(X) = P(A1*A2), P(A1*A2) = P(A1) * P(A2|A1).
            P(A2|A1) = 1/(n - (i-1)).
            =>
            P(X) = ((n - (i-1))!/n!) * (1/(n - (i-1))) = (n-i)!/n!.
         */
    }

    std::set<int> RandomSample(int m, int n)
    {
        if((n <= 0) || (m <= 0) || (m > n))
        {
            return {};
        }

        std::set<int> sample;

        std::random_device device;
        std::mt19937 generator(device());

        /*
         Loop invariant:
            before each i-iteration probability that subarray arr[0, i-1] has certain permutation is (n - (i-1))!/n!
            (i.e. (i-1)-permutations are uniforml y distributed).
         */
        for(int i = m - 1; i >= 0; i--)
        {
            std::uniform_int_distribution<int> distribution{0, (n - 1) - i};
            int value = distribution(generator);
            if(sample.find(value) != sample.end())
            {
                sample.insert((n - 1) - i);
            }
            else
            {
                sample.insert(value);
            }
        }

        // Result sample is uniformly distributed.
        return sample;
    }

    std::vector<int> RandomSample(const std::vector<int> &arr, size_t m)
    {
        std::vector<int> sample;

        std::set<int> indexes = RandomSample(static_cast<int>(m), static_cast<int>(arr.size()));
        for(int index : indexes)
        {
            sample.push_back(arr[index]);
        }

        return sample;
    }

    std::pair<size_t, size_t> FindTerms(const std::vector<int> &arr, int value)
    {
        size_t n = arr.size();
        size_t i = 0;
        size_t j = n - 1;

        // Loop invariant: For each I < i, J > j: arr[I] + arr[J] != value.
        while(i < j)
        {
            int sum = arr[i] + arr[j];
            if(sum < value)
            {
                ++i;
            }
            else if (sum > value)
            {
                --j;
            }
            else
            {
                return std::make_pair(i, j);
            }
        }

        return std::make_pair(n, n);
    }

    size_t CountInversions(const std::vector<int> &arr)
    {
        std::vector<int> arrCopy{arr.begin(), arr.end()};
        std::vector<int> buffer;
        buffer.reserve(arr.size());
        return SAlgorithms::Inversions::CountInversions(arrCopy, 0, arr.size(), buffer);
    }

    std::pair<size_t, size_t> FindMaxSubarray(const std::vector<int> &arr)
    {
        size_t maxSumBegin = 0;
        size_t maxSumEnd = 1;
        int maxSum = arr[0];

        size_t currentBegin = maxSumBegin;
        int current = maxSum;

        /*
            Loop invariant: current - maximal sum of all sub-arryas of the form a[k..i-1], where 0 <= k <= j-1.
        */
        for(int i = 1; i < arr.size(); ++i)
        {
            int value = arr[i];

            bool restart = current + value < value;
            current = std::max(value, current + value);

            if(restart)
            {
                currentBegin = i;
            }

            if(current > maxSum)
            {
                maxSumBegin = currentBegin;
                maxSumEnd = i + 1;
                maxSum = current;
            }
        }

        /*
            'maxSum' is the maximal sum because we calculated is as the maximal value between all summs of maximal
            subarrays of type A[i,j], for each j in [0, n -1].
         */

        return std::make_pair(maxSumBegin, maxSumEnd);
    }

    namespace Suboptimal
    {
        namespace SMaxSubarrayDivide
        {
            std::tuple<size_t, size_t, int> FindMaxInMiddle(
                                                            const std::vector<int> &arr,
                                                            size_t begin,
                                                            size_t mid,
                                                            size_t end)
            {
                size_t leftMaxIndex = mid - 1;
                int leftSum = arr[leftMaxIndex];

                int sum = leftSum;
                size_t i = leftMaxIndex;
                if(leftMaxIndex > begin)
                {
                    do
                    {
                        --i;
                        sum += arr[i];
                        if(sum > leftSum)
                        {
                            leftSum = sum;
                            leftMaxIndex = i;
                        }
                    } while(i > begin);
                }

                size_t rightMaxIndex = mid;
                int rightSum = arr[rightMaxIndex];

                sum = rightSum;
                i = rightMaxIndex + 1;
                while(i < end)
                {
                    sum += arr[i];
                    if(sum > rightSum)
                    {
                        rightSum = sum;
                        rightMaxIndex = i;
                    }

                    ++i;
                }

                return std::make_tuple(leftMaxIndex, rightMaxIndex + 1, leftSum + rightSum);
            }

            std::tuple<size_t, size_t, int> FindMaxSubarray(
                                                            const std::vector<int> &arr,
                                                            size_t begin,
                                                            size_t end)
            {
                if(end - begin > 1)
                {
                    size_t mid = (begin + end) / 2;
                    std::tuple<size_t, size_t, int> onLeft = FindMaxSubarray(arr, begin, mid);
                    std::tuple<size_t, size_t, int> onRight = FindMaxSubarray(arr, mid, end);
                    std::tuple<size_t, size_t, int> inMid = FindMaxInMiddle(arr, begin, mid, end);

                    int leftSum = std::get<2>(onLeft);
                    int rightSum = std::get<2>(onRight);
                    int midSum = std::get<2>(inMid);

                    if((leftSum >= rightSum) && (leftSum >= midSum))
                    {
                        return onLeft;
                    }
                    else if((rightSum >= leftSum) && (rightSum >= midSum))
                    {
                        return onRight;
                    }
                    else
                    {
                        return inMid;
                    }

                }

                return std::make_tuple(begin, end, arr[begin]);
            }
        }

        std::pair<size_t, size_t> FindMaxSubarrayDivide(const std::vector<int> &arr)
        {
            std::tuple<size_t, size_t, int> max = SMaxSubarrayDivide::FindMaxSubarray(arr, 0, arr.size());
            return std::make_pair(std::get<0>(max), std::get<1>(max));
        }

        std::pair<size_t, size_t> FindMaxSubarrayBruteForce(const std::vector<int> &arr)
        {
            size_t n = arr.size();
            size_t maxSumBegin = 0;
            size_t maxSumEnd = n;
            int maxSum = SAlgorithms::SumRange(arr, 0, n);

            for(size_t subSize = 1; subSize < n; ++subSize)
            {
                int sum = SAlgorithms::SumRange(arr, 0, subSize);
                if(sum > maxSum)
                {
                    maxSumBegin = 0;
                    maxSumEnd = subSize;
                    maxSum = sum;
                }

                for(size_t i = 1; i < (n - subSize + 1); ++i)
                {
                    sum += (arr[i + subSize - 1] - arr[i - 1]);
                    if(sum > maxSum)
                    {
                        maxSumBegin = i;
                        maxSumEnd = i + subSize;
                        maxSum = sum;
                    }
                }
            }

            return std::make_pair(maxSumBegin, maxSumEnd);
        }
    }
}
