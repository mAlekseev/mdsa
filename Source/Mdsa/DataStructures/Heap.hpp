//
//  Heap.hpp
//  mdsa
//
//  Created by moxxim on 3/25/20.
//  Copyright © 2020 moxxim. All rights reserved.
//

#ifndef Heap_hpp
#define Heap_hpp

#include <vector>

namespace Mdsa
{
    enum class HeapType : uint8_t
    {
        Max = 0,
        Min = 1,
    };

    /*
        \brief Ensures that the root with only its direct children is a heap.
        \complexity O(lg(n)): equation is "T(n) <= T(2/3n) + 0(1)".
     */
    void Heapify(HeapType type, std::vector<int> &arr, size_t heapSize, size_t root);

    /*
        \brief Makes an array a heap.
        \complexity is O(n).
     */
    void MakeHeap(HeapType heapType, std::vector<int> &arr);
}

#endif /* Heap_hpp */
