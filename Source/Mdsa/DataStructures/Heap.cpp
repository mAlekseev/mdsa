//
//  Heap.cpp
//  mdsa
//
//  Created by moxxim on 3/25/20.
//  Copyright © 2020 moxxim. All rights reserved.
//

#include "Heap.hpp"

namespace Mdsa
{
    namespace SHeap
    {
        inline size_t GetNodeParent(size_t childIndex)
        {
            return ((childIndex + 1) / 2) - 1;
        }

        inline size_t GetLeftChild(size_t parentIndex)
        {
            return (parentIndex * 2) + 1;
        }

        inline size_t GetRightChild(size_t parentIndex)
        {
            return (parentIndex * 2) + 2;
        }
    }

    void Heapify(HeapType type, std::vector<int> &arr, size_t heapSize, size_t root)
    {
        size_t left = SHeap::GetLeftChild(root);
        size_t right = SHeap::GetRightChild(root);
        auto needSwap = [&arr, type](size_t current, size_t possible)
        {
            return (type == HeapType::Max) ? (arr[possible] > arr[current]) : (arr[possible] < arr[current]);
        };

        size_t extreme = ((left < heapSize) && needSwap(root, left)) ? left : root;
        if((right < heapSize) && needSwap(extreme, right))
        {
            extreme = right;
        }

        if(extreme != root)
        {
            std::swap(arr[root], arr[extreme]);
            Heapify(type, arr, heapSize, extreme);
        }
    }

    void MakeHeap(HeapType heapType, std::vector<int> &arr)
    {
        size_t size = arr.size();
        if(size > 1)
        {
            /*
                Loop invariant: before each iteration any sub-tree with roots at [i + 1, i + 2, .. n]
                                is a heap of desired type.
            */
            size_t i = size / 2;
            do
            {
                --i;
                Heapify(heapType, arr, size, i);
            }
            while(i > 0);
        }
    }
}
